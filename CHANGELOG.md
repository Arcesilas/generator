# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

Please see [TODO.md] for planned features.

## [Unreleased]

## [1.1.0] - 2018-03-30
### Added
- Default paths
- `classGenerator` option for Generator generators
- `builtin-generators` option to enable only the required ones

### Fixed
- If ClassGenerator cannot find composer.json, an exception is thrown
- Suffix is correctly handled

### Changed
- Documentation has been rewritten

## [1.0.1] - 2018-02-03
- Fixed Scrutinizer configuration

## [1.0.0] - 2018-02-02
First release
