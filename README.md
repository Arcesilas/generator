# Arcesilas/Generator

This package helps you generate files and classes from stubs for your projects. Tired to always have to write the same lines of code for every TestCase you need? Generate them automatically and just write the actual test!

> This package uses the term `classPath` which represent the path of a class in a PSR-4 filesystem. For example, if your App is in namespace `App\` which is in `src/App/`, then the classPath `src/App/Controllers/HomeController` represents the class HomeController which namespace is `App\Controllers`. The file of this class is `src/App/Controllers/HomeController.php`.

Table of contents:

- [Installation](doc/installation.md)
- [Usage](doc/usage.md)
- [Configuration](doc/configuration.md)
- [Writing generators](doc/writing-generators.md)
