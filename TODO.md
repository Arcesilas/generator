# TODO

## Info command

Add an `info` command to display information about a generator (including options the generator accepts)

# Raw list

Add a --raw option to ListCommand, to display the list in a format that can be easily programmatically read

# Configurator

Add a command to add generators to config file. This command could be run after a generator is generated.

# Aliases

Allow user to define generators aliases in configuration file. An alias can be a shortcut to run a specific generator with options.

# Default root

Add configuration section to allow user to define default root for each generator. For instance, tests may always be written in `tests/` directory.
