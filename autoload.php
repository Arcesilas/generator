<?php

// Find autoload
$paths = [
    dirname($_SERVER['PHP_SELF']).'/../autoload.php',
    __DIR__.'/vendor/autoload.php',
];

foreach ($paths as $path) {
    if (file_exists($path)) {
        require $path;
        return;
    }
}

echo "Could not find autoload.php".PHP_EOL;
exit(1);
