# Builtin generators

The package provides some builtin generators to make your coder's life easier:

- Class generator
- Command generator (for Symfony/Console)
- Regex generator generator (makes a `RegexGenerator`)
- ServiceProvider generator (for League/Container)
- Testcase generator (for PHPUnit)

Some of them accept options, see below.

## Class Generator

The class generator can also generate abstract classes, interfaces and traits.

If the filename follows the common rule:
- AbstractFoo
- FooInterface
- FooTrait

then the class type will be automatically detected. You may override this using the `--option` (short: `-o`) command line option. For example:

```shell
generator make FooBar -o interface
```

The supported types are obviously: `abstract`, `interface` and `trait`.

## ServiceProvider Generator

ServiceProviders for `League/Container` can optionnaly be bootable. The generator can make a bootable provider, when using the `bootable` option on the command line:

```shell
generator make service-provider Providers/FooServiceProvider -o bootable
```

## Generator generators

The package provides 2 generators generators (sic): generators which make generators! :wink:

They both accept a `classGenerator` option, if you want to make a Class Generator:

```shell
generator make regex-generator app/Generators/FooGenerator -o classGenerator
```
