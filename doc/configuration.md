# Configuration

The configuration filename is `.generator.yml`. It is expected to be found in the current working directory, along with `composer.json`.

The package provides an example configuration file `.generator.example.yml` with default values: you may copy and edit it in your project.

- [edit](#edit)
- [editor](#editor)
- [suffix](#suffix)
- [permissions](#permissions)
- [paths](#paths)
- [builtin-generators](#builtin-generators)
- [generators](#generators)

## edit

- boolean
- default : false

Whether to edit or not the generated file before it is written.

There is also a `--edit` (short: `-e`) option for the command line that will override this option.

If you use `--edit` and `--no-interaction` options on the command line, `--edit` will prevail. If you specify `edit: true` in the configuration file and `--no-interaction` on the command line, `--no-interaction` will prevail.

## editor

- string
- default: $EDITOR (depends on your environment)

If you don't specify this option and use `edit`, the generator will try to find the editor configured in your environment. If none is found, an error will be raised.

You can specify a CLI editor (eg: VI, VIM, Nano) or a GUI editor (eg: gedit, mousepad).

>  :warning: Some issues have been experienced with GUI editors: the generator may run the editor but not wait for it to end to continue. This would prevent you from editing the generated file, though it would be correctly written (without your modifications).

### suffix
- string
- default: .php

You may create files with another suffix than `.php`. This option is also available on the command line for each file you generate. You may forget the `.`, it will be added if necessary.

### permissions
- integer
There are two permissions defined: for directories and files.

Defaults:
- **dir**: 0755
- **file**: 0644    

### paths
- list
- default: none

You can specify a default path for the generated files. Simply associate the template name and the path (relative to the current working directory). For example, if you configure:

```yaml
paths:
    - class: src/
```

then the following command:

```shell
generator make class FooBar
```

will generate the `./src/FooBar.php` file.

## builtin-generators
- list
- default: none

This is a whitelist of the builtin generators you want to use. Specify the template names, not the classes

Example:

```yaml
builtin-generators: [class, test]
```

The only generators loaded will then be `Arcesilas\Generator\Builtin\ClassGenerator` and `Arcesilas\Generator\Builtin\PHPUnitTestCaseGenerator`.

## generators
- list
- default: none

This is where you can specify your own generators. You may also override the builtin ones if you like.

The index of the list is the name of the generator, and the value is the FQCN (*Fully Qualified Class Name*) of the generator you want to use. For example:

```yaml
generators:
    test: App\Generators\AtoumTestGenerator
    view: App\Generators\ViewGenerator
```

will result in the following available generators:

```shell
generator list

Available generators
====================

 --------- -----------------------------------------------------
  Name      Class                                                
 --------- -----------------------------------------------------
  command   Arcesilas\Generator\Builtin\CommandGenerator          
  ...
  test      App\Generators\AtoumTestGenerator
  view      App\Generators\ViewGenerator
 --------- -----------------------------------------------------
```

The `test` generator has been overridden, the `view` generator has been added and the others have been left unmodified.

> :information_source: You may "*rename*" a generator by not adding it to the `builtin-generators` whitelist and adding it to the `generators` list with another name.

> For example, if you consider that `service-provider` is too long, you can configure the generator this way:

> ```yaml
> builtin-generators:
>   - class
>   - test
>   # Add other generators you need, except service-provider
> generators:
>   - sp: Arcesilas\Generator\Builtin\ServiceProviderGenerator
> ```

> Now, to generate a service provider, just run:

> ```shell
> generator make sp fooProvider
> ```
