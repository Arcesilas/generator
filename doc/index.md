# Generator

- [Installation](installation.md)
- [Usage](usage.md)
- [Configuration](configuration.md)
- Builtin generators
    - [Class generator](builtin-generators/class-generator.md)
    - [Command generator (for Symfony/Console)](builtin-generators/command-generator.md)
    - [Regex generator generator (makes a `RegexGenerator`)](builtin-generators/regex-generator-generator.md)
    - [ServiceProvider generator (for League/Container)](builtin-generators/service-provider-generator.md)
    - [Testcase generator (for PHPUnit)](builtin-generators/phpunit-testcase-generator.md)
