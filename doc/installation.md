# Installation

## Using Composer

Simply run:

```shell
composer require Arcesilas/generator
```

You may install it globally in order to make it available in any of your projects:

```shell
composer global require Arcesilas/generator
```

> Make sure that the `~/.composer/vendor/bin` directory is added to your `PATH`, so that the generator executable can be located by your system.

## Using git

Clone the repository:

```shell
git clone https://github.com/Arcesilas/generator.git
```

Add the `bin` directory to your path

```shell
PATH="/path/to/generator/bin:$PATH"
```

or create a bash alias:

```shell
alias generator='/path/to/generator/bin/generator'
```
