# Usage

The `generator` script is intended to be run at the root of your project, where `composer.json` can be found.

> :warning: If `composer.json` cannot be found, the script will stop with an error.

## List generators

To list available generators, simply rin the `list` command. The package comes with some builtin generators. By default the available generators are:

```shell
generator list

Available generators
====================

 ------------------ ----------------------------------------------------- --------------------------------------------------
  Name               Class                                                 Description                                       
 ------------------ ----------------------------------------------------- --------------------------------------------------
  class              Arcesilas\Generator\Builtin\ClassGenerator             Generates a basic class                           
  command            Arcesilas\Generator\Builtin\CommandGenerator           Generates a command class for Symfony/Console     
  php-generator      Arcesilas\Generator\Builtin\PhpGeneratorGenerator      Generates a php based generator                   
  regex-generator    Arcesilas\Generator\Builtin\RegexGeneratorGenerator    Generates a regex based generator                 
  service-provider   Arcesilas\Generator\Builtin\ServiceProviderGenerator   Generates a ServiceProvider for League/Container  
  test               Arcesilas\Generator\Builtin\PHPUnitTestCaseGenerator   Generates a PHPUnit TestCase                      
 ------------------ ----------------------------------------------------- --------------------------------------------------
```

You may use the `--all` option (short: `-a`) to list all generators, including invalid ones (if generator class does not exist).

> :information_source: Tip: You can enable only some of your choice (or disable all of them). See [configuration](configuration.md#builtin-generators).

## Make a class

To create a new file, simply run the `make` command. For example, to create a PHPUnit TestCase:

```shell
generator make test tests/Controllers/HomeControllerTest
```

The file `tests/Controllers/HomeControllerTest.php` will be created with a `HomeControllerTest` class which extends `PHPUnit\Framework\TestCase`. You just have to write your tests.

Some Generators accept options. See the [builtin generators documention](builtin-generators.md).

### Command line options

* --edit, -e

Allow you to edit the file after it has been generated and before it is written. If `--editor` is not specified, the default editor will be used, depending on your environment configuration.

* --editor

Specify the editor you wish to use to edit the temporary file before it is written. Example:

```shell
generator make class FooClass --editor=vim
```

* --option, -o

Additional option passed to the generator.

For example, this command line will generate an interface instead of a concrete class:

```shell
generator make class FooClass -o interface
```

Additional option value may be passed along with the name, separated by a colon:

```shell
generator make something SomeFile -o foo:bar
```

* --suffix, -s

Use a specific suffic for the generated files. Default is .php
