# Writing generators

1. [Anatomy of the generators](#anatomy-of-the-generators)
    - [`GeneratorInterface`](#generatorinterface)
    - [`ClassGeneratorInterface`](#classgeneratorinterface)
    - [`BaseGenerator` abstract class](#basegenerator-abstract-class)
    - The [`stubData`](#the-stubdata-property) property
2. [The base generator classes](#the-base-generator-classes)
3. [The generators generators](#the-generators-generators)
    - `PhpGeneratorGenerator`
    - `RegexGeneratorGenerator`

Yeah... many generators :)

## Anatomy of the generators

### `GeneratorInterface`

All generators MUST implement the `GeneratorInterface`, which defines 4 methods:

```php
public function getStub(): string;
public function checkFileDoesNotExist();
public function setPermissions($dir = 0755, $file = 0644);
public function write($content);
```

### `ClassGeneratorInterface`

Generators which implements this interface receive a `ComposerJson` object, which gives access to useful data about PSR-4 autoloading (namespaces and paths).

The `ClassGeneratorInterface` defines one method:

```php
public function setComposerJson(ComposerJson $composerJson);
```

You may use the `ClassGeneratorTrait` to easily implement the interface.

### `BaseGenerator` abstract class

The `BaseGenerator` abstract class implements 3 of the 4 methods from `GeneratorInterface`:

```php
public function checkFileDoesNotExist();
public function setPermissions($dir = 0755, $file = 0644);
public function write($content);
```

Your generator has to implement the `getStub` method, which role is to:
- read the stub (template) file
- replace the data in the template
- return the contents of the file, ready to be edited and written

### The `stubData` property

The data required for the file generation is stored in the `stubData` property of the `BaseGenerator`.

It contains:
- `filepath`: the path of the file that will be created (this is the `name` argument of the `make` command)
- `dirname`: the directory in which the file will be written
- the options passed with the `--option` command line option

Generators which implement the `ClassGeneratorInterface` also receive the result of `ComposerJson::classinfo()` method:
- `namespace`: the namespace of the class (for `ClassGenerator`s)
- `class`: the name of the class

The `ClassGenerator` also has a `type` in the `stubData` property, since this generator allows to generate abstract classes, interfaces and traits. See the [`ClassGenerator` documentation](builtin-generators#class-generator).

## The base generator classes

The `PhpGenerator` and `RegexGenerator` abstract classes use respectively PHP and Regex to render the file, ie to fill the template with the appropriate data.

> When using the `PhpGenerator`, it is important to note that when generating a PHP file, the PHP open tag `<?php` must be printed/echoed. Example:

> ```php
> <?= '<?php' ?>
> ```

### The generators generators

Since this package is a file generator, it's worth using it to make new generators...

To help you create simple generators, you can use the two `PhpGeneratorGenerator` and `RegexGeneratorGenerator` which obviously create generators that extend resp. `PhpGenerator` and `RegexGenerators`.

```shell
generator make php-generator app/Generators/LicenseGenerator
```

To complete this new `LicenseGenerator`, you then just have to set the description and the path to the stub.

If you want to generate a class generator, pass the `classGenerator` as an option on the command line:

```shell
generator make php-generator app/Generator/ControllerGenerator -o classGenerator
```
