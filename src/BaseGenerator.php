<?php
declare(strict_types=1);

namespace Arcesilas\Generator;

use Arcesilas\Generator\Config\ConfigInterface;
use Arcesilas\Generator\GeneratorInterface;
use Arcesilas\Generator\GeneratorException;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\Util\ComposerJsonException;

abstract class BaseGenerator implements GeneratorInterface
{
    /**
     * Generator description. Should be overridden by concrete generators
     * @var string
     */
    public static $description = '';

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * Template file to generate the class/file
     * @var string
     */
    protected $stub;

    /**
     * Created files suffix. Dot may be ommitted.
     * @var string
     */
    protected $suffix = '.php';

    /**
     * Data available in the stub
     *
     * @var array
     */
    protected $stubData;

    /**
     * Permissions of created directories and files
     * @var array
     */
    protected $permissions = [
        'dir' => 0755,
        'file' => 0644
    ];

    /**
     * The filepath is a relative path on the filesystem to the class (not the file)
     * For instance, `src/App/Controllers/HomeController` is the path to
     * `App\Controllers\HomeController` class in `/src/App/Controllers/HomeController.php` file
     * @param  ConfigInterface $config
     * @param  string          $filepath
     * @param  array           $options
     */
    public function __construct(ConfigInterface $config, string $filepath, array $options)
    {
        $this->config = $config;
        $this->stubData = [
            'filepath' => $filepath,
            'dirname' => dirname(getcwd().'/'.$filepath)
        ];
        $this->setOptions($options);
    }

    /**
     * Set permissions for directories and files creation
     *
     * @param  int        $dir
     * @param  int        $file
     */
    final public function setPermissions($dir = 0755, $file = 0644)
    {
        $this->permissions = [
            'dir' => $dir,
            'file' => $file
        ];
    }

    /**
     * Write the content to destination file
     * Content argument is required since user may have edited the file after generation and before save
     *
     * @param  string $content
     */
    public function write($content)
    {
        $this->mkdir($this->getDir());

        $success = @file_put_contents($this->getFile(), $content);
        if (false === $success) {
            throw new GeneratorException(
                sprintf('Could not write to %s file', $this->getFile()),
                GeneratorException::FILE_NOT_WRITTEN
            );
        }
        chmod($this->getFile(), $this->permissions['file']);
    }

    /**
     * Check if file to write exists or not
     *
     * @throws GeneratorException
     */
    public function checkFileDoesNotExist()
    {
        if (file_exists($this->getFile())) {
            throw new GeneratorException(
                sprintf('File %s already exists', $this->getFile()),
                GeneratorException::FILE_ALREADY_EXISTS
            );
        }
    }

    /**
     * Recursively create the specified directory and apply the permission
     *
     * @param  string $path
     */
    protected function mkdir(string $path)
    {
        if (!file_exists($path) && !is_dir($path)) {
            $umask = umask(0);
            if (false === @mkdir($path, $this->permissions['dir'], true)) {
                throw new GeneratorException(
                    'Could not create directory '.$path,
                    GeneratorException::DIRECTORY_NOT_CREATED
                );
            }
            umask($umask);
        }
    }

    /**
     * Return the file to write
     *
     * @return string
     */
    public function getFile(): string
    {
        $suffix = $this->config->get('suffix', $this->suffix);
        return $this->stubData['filepath'].'.'.ltrim($suffix, '.');
    }

    /**
     * Return the directory where to write the file
     *
     * @return string
     */
    public function getDir(): string
    {
        return $this->stubData['dirname'];
    }

    /**
     * Set the command line options on the generator
     *
     * @param  array      $options
     */
    protected function setOptions(array $options)
    {
        foreach ($options as $option) {
            $pos = strpos(trim($option, ':'), ':');

            if (false === $pos) {
                $this->stubData[$option] = true;
            } else {
                list($key, $value) = explode(':', $option, 2);
                $this->stubData[$key] = $value;
            }
        }
    }
}
