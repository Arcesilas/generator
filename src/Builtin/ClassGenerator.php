<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Builtin;

use Arcesilas\Generator\Config\ConfigInterface;
use Arcesilas\Generator\RegexGenerator;
use Arcesilas\Generator\ClassGeneratorInterface;
use Arcesilas\Generator\Traits\ClassGeneratorTrait;

class ClassGenerator extends RegexGenerator implements ClassGeneratorInterface
{
    use ClassGeneratorTrait;

    /**
     * {@inheritdoc}
     */
    public static $description = 'Generates a basic class';

    /**
     * {@inheritdoc}
     */
    protected $stub = __DIR__.'/stubs/Class.stub';

    /**
     * Class types handled
     * @var array
     */
    protected $types = [
        'abstract'  => 'abstract class',
        'interface' => 'interface',
        'trait'     => 'trait'
    ];

    /**
     * Patterns for class type autodetection
     * @var array
     */
    protected $typesPatterns = [
        'abstract'  => '^Abstract',
        'interface' => 'Interface$',
        'trait'     => 'Trait$'
    ];

    /**
     * {@inheritdoc}
     */
    public function getStub(): string
    {
        $this->setType();
        return parent::getStub();
    }

    /**
     * Set the type of class generated (class, trait, interface)
     */
    protected function setType()
    {
        // Was the type given in options?
        foreach ($this->stubData as $name => $value) {
            if (array_key_exists(strtolower($name), $this->types) && true === $value) {
                $this->stubData['type'] = $this->types[$name];
                unset($this->stubData[$name]);
                return;
            }
        }

        // Automatically detect the type from the file name
        foreach ($this->types as $type => $classType) {
            $pattern = sprintf('`%s`', $this->typesPatterns[$type]);
            if (preg_match($pattern, $this->stubData['class'])) {
                $this->stubData['type'] = $classType;
                return;
            }
        }

        // Default to `class`
        $this->stubData['type'] = 'class';
    }
}
