<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Builtin;

use Arcesilas\Generator\RegexGenerator;
use Arcesilas\Generator\ClassGeneratorInterface;
use Arcesilas\Generator\Traits\ClassGeneratorTrait;

class PHPUnitTestCaseGenerator extends RegexGenerator implements ClassGeneratorInterface
{
    use ClassGeneratorTrait;

    /**
     * {@inheritdoc}
     */
    public static $description = 'Generates a PHPUnit TestCase';

    /**
     * {@inheritdoc}
     */
    protected $stub = __DIR__.'/stubs/PHPUnitTestCase.stub';
}
