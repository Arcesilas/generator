<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Builtin;

use Arcesilas\Generator\PhpGenerator;
use Arcesilas\Generator\ClassGeneratorInterface;
use Arcesilas\Generator\Traits\ClassGeneratorTrait;

class RegexGeneratorGenerator extends PhpGenerator implements ClassGeneratorInterface
{
    use ClassGeneratorTrait;

    /**
     * {@inheritdoc}
     */
    public static $description = 'Generates a regex based generator';

    /**
     * {@inheritdoc}
     */
    protected $stub = __DIR__.'/stubs/RegexGenerator.stub';
}
