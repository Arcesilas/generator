<?php

namespace Arcesilas\Generator;

use Arcesilas\Generator\Util\ComposerJson;

interface ClassGeneratorInterface
{
    public function setComposerJson(ComposerJson $composerJson);
}
