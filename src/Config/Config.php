<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Config;

use Dflydev\DotAccessData\Data;
use Symfony\Component\Yaml\Yaml;

class Config implements ConfigInterface
{
    /**
     * @var Data
     */
    protected $data;

    /**
     * @param  string      $filepath Path to configuration file
     */
    public function __construct(string $filepath = null)
    {
        $this->data = new Data;
        $filepath = $filepath ?? getcwd().'/.generator.yml';
        if (file_exists($filepath)) {
            $this->addConfig($filepath);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addConfig(string $filepath)
    {
        $config = Yaml::parseFile($filepath) ?: [];
        $this->data->import($config);
    }

    public function set(array $values)
    {
        foreach ($values as $name => $value) {
            $this->data->set($name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $default = null)
    {
        return $this->data->get($key, $default);
    }
}
