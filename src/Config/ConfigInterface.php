<?php

namespace Arcesilas\Generator\Config;

interface ConfigInterface
{
    /**
     * Load the configuration
     * @param  string     $file
     * @return void
     */
    public function addConfig(string $file);

    /**
     * Return a configuration item value
     * @param  string $name
     * @param  mixed  $default
     * @return mixed
     */
    public function get(string $name, $default = null);

    /**
     * Return a configuration item value
     * @param  string $name
     * @return mixed
     */
    public function __get(string $name);

    /**
     * Set a configuration item
     * @param  array  $values
     */
    public function set(array $values);
}
