<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console;

use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Command\HelpCommand;
use Arcesilas\Generator\Console\Command\UsageCommand;

class Application extends ConsoleApplication
{
    /**
     * {@inheritdoc}
     */
    protected function getDefaultCommands(): array
    {
        return array(new HelpCommand(), new UsageCommand());
    }
}
