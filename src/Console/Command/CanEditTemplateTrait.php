<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Yaml\Exception\RuntimeException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Arcesilas\Generator\GeneratorException;

trait CanEditTemplateTrait
{
    /**
     * Warnings to display to user, if any
     * @var string[]
     */
    protected $warnings = [];

    /**
     * Configure command to add edition capabilities
     */
    protected function configureEdition()
    {
        $this->addOption(
            'edit',
            'e',
            InputOption::VALUE_NONE,
            'Edit the file before saving it'
        )->addOption(
            'editor',
            null,
            InputOption::VALUE_REQUIRED,
            'Editor for file editing (when using --edit option)'
        );
    }

    /**
     * Whether to edit the generated file before saving it or not
     *
     * @return bool
     */
    protected function editBeforeWrite(): bool
    {
        return $this->input->getOption('edit')
            || ($this->config->get('edit') && ! $this->input->getOption('no-interaction'));
    }

    /**
     * Returns the command to run editor
     *
     * @param  string           $filename
     * @return string
     */
    protected function getEditorCommand(string $filename): string
    {
        $editor = $this->getEditor();

        return "$editor $filename";
    }

    /**
     * Returns the editor based on environment, configuration and command line option
     *
     * @return string
     * @throws RuntimeException
     */
    protected function getEditor(): string
    {
        $editor = $this->input->getOption('editor') ?? $this->config->get('editor', getenv('EDITOR'));

        if (!$editor) {
            throw new GeneratorException(
                'Could not find any editor to edit file',
                GeneratorException::EDITOR_NOT_FOUND
            );
        }

        return $editor;
    }

    /**
     * Actually edit the stub in a temporary file before writing it to final destination file
     *
     * @param  string      $fileContent
     * @return string
     */
    protected function editTmpFile(string $fileContent): string
    {
        $tmpFile = tempnam(sys_get_temp_dir(), "generator-");

        file_put_contents($tmpFile, $fileContent);

        $process = new Process($this->getEditorCommand($tmpFile));
        $process->disableOutput();

        try {
            $process->setTty(true);
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            $this->warnings[] = $e->getMessage();
        }

        return file_get_contents($tmpFile);
    }
}
