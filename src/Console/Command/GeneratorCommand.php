<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Arcesilas\Generator\Config\ConfigInterface;
use Arcesilas\Generator\GeneratorFactory;

abstract class GeneratorCommand extends Command
{
    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var GeneratorFactory
     */
    protected $factory;

    /**
     * Output with style
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * {@inheritdoc}
     *
     * @param  string           $name
     * @param  ConfigInterface  $config
     * @param  GeneratorFactory $factory
     */
    public function __construct(ConfigInterface $config, GeneratorFactory $factory)
    {
        parent::__construct();
        $this->config = $config;
        $this->factory = $factory;
    }

    /**
     * {@inheridtoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);
        // Override configuration with command line options
        $this->config->set($input->getOptions());
    }
}
