<?php

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LaCommand extends Command
{
    protected function configure()
    {
        $this->setName('la')
            ->setDescription('Alias of <fg=green>list --all</>');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listAll = $this->getApplication()->find('list');
        $input = new ArrayInput(['--all' => true]);
        return $listAll->run($input, $output);
    }
}
