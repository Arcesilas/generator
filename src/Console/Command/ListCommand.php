<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Arcesilas\Generator\GeneratorInterface;

class ListCommand extends GeneratorCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('list')
            ->setDescription('List available generators')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'List all generators, even the invalid ones');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $generators = $this->factory->getDefinedGenerators();

        $this->io->title('Available generators');
        $headers = [
            true => ['Valid', 'Name', 'Class', 'Description'],
            false => ['Name', 'Class', 'Description']
        ];

        $all = $input->getOption('all');
        $method = $all ? 'listAll' : 'listOnlyValid';

        $this->io->table(
            $headers[$all],
            array_map([$this, $method], array_keys($generators), array_values($generators))
        );
    }

    /**
     * Check whether a class is a valid generator or not
     * @param  string      $class
     * @return bool
     */
    protected function isValidGenerator(string $class)
    {
        return class_exists($class) && in_array(GeneratorInterface::class, class_implements($class));
    }

    /**
     * Return generator info to display all of them
     * @param  string  $name
     * @param  string  $class
     * @return array
     */
    protected function listAll(string $name, string $class)
    {
        $valid = $this->isValidGenerator($class);
        return [
            $valid ? '  <fg=green>✔</>' : '  <fg=red>✘</>',
            $name,
            $class,
            $valid ? $class::$description : ''
        ];
    }

    /**
     * Return generator info to display only valid ones
     * @param  string        $name
     * @param  string        $class
     * @return array
     */
    protected function listOnlyValid(string $name, string $class)
    {
        if (!$this->isValidGenerator($class)) {
            return [];
        }

        return [$name, $class, $class::$description];
    }
}
