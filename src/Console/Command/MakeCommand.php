<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Arcesilas\Generator\GeneratorException;

class MakeCommand extends GeneratorCommand
{
    use CanEditTemplateTrait;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('make')
            ->setDescription('Creates a new class.')
            ->addArgument('template', InputArgument::REQUIRED, 'Template to use for the class')
            ->addArgument('name', InputArgument::REQUIRED, 'Class name')
            ->addOption('suffix', 's', InputOption::VALUE_REQUIRED, 'Suffix for created files (default: .php)')
            ->addOption(
                'option',
                'o',
                InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
                'Additional options to be passed to the generator',
                []
            );

        $this->configureEdition();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $generator = $this->factory->makeGenerator(
                $input->getArgument('template'),
                $input->getArgument('name'),
                $input->getOption('option')
            );
            $generator->checkFileDoesNotExist();

            $content = $generator->getStub();

            if ($this->editBeforeWrite()) {
                $content = $this->editTmpFile($content);
            }

            $generator->write($content);
            $this->io->success(sprintf(
                'Class %s has been successfully created',
                $generator->getFile()
            ));
        } catch (GeneratorException $e) {
            $this->io->error($e->getMessage());
            return $e->getCode();
        } finally {
            if (! empty($this->warnings)) {
                foreach ($this->warnings as $warning) {
                    $this->io->warning($warning);
                }
            }

        }
    }
}
