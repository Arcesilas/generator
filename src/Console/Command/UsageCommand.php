<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Console\Command;

use Symfony\Component\Console\Command\ListCommand as SfListCommand;

class UsageCommand extends SfListCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this->setName('usage');
    }
}
