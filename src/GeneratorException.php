<?php

namespace Arcesilas\Generator;

class GeneratorException extends \RuntimeException
{
    /**
     * Generator was not found
     * @used-by GeneratorFactory::makeGenerator()
     * @var int
     */
    const GENERATOR_NOT_FOUND      = 1;

    /**
     * Class is not an instance of GeneratorInterface
     * @used-by GeneratorFactory::makeGenerator()
     * @var int
     */
    const NOT_GENERATOR_INSTANCE   = 2;

    /**
     * Generator was not written
     * @used-by BaseGenerator::write()
     * @var int
     */
    const FILE_NOT_WRITTEN         = 3;

    /**
     * Generator does not exist
     * @used-by GeneratorFactory::makeGenerator()
     * @var int
     */
    const GENERATOR_DOES_NOT_EXIST = 4;

    /**
     * The file to create already exists
     * @used-by BaseGenerator::checkFileDoesNotExist()
     * @var int
     */
    const FILE_ALREADY_EXISTS = 5;

    /**
     * Could not find namespace in composer.json
     * @use-by BaseGenerator::__construct()
     * @var int
     */
    const NAMESPACE_NOT_FOUND = 6;

    /**
     * Could not find an editor in options, config or environment variables
     * @used-by CanEditTemplateTrait::getEditor()
     * @var int
     */
    const EDITOR_NOT_FOUND = 7;

    /**
     * Could not create target directory
     * @used-by BaseGenerator::mkdir()
     * @var integer
     */
    const DIRECTORY_NOT_CREATED = 8;
}
