<?php
declare(strict_types=1);

namespace Arcesilas\Generator;

use Arcesilas\Generator\Config\ConfigInterface;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\Util\ComposerJsonException;
use Arcesilas\Generator\GeneratorException;

class GeneratorFactory
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var ComposerJson
     */
    protected $composerJson;

    /**
     * @var array
     */
    private $builtinGenerators = [
        'test' => __NAMESPACE__.'\\Builtin\\PHPUnitTestCaseGenerator',
        'command' => __NAMESPACE__.'\\Builtin\\CommandGenerator',
        'class' => __NAMESPACE__.'\\Builtin\\ClassGenerator',
        'php-generator' => __NAMESPACE__.'\\Builtin\\PhpGeneratorGenerator',
        'regex-generator' => __NAMESPACE__.'\\Builtin\\RegexGeneratorGenerator',
        'service-provider' => __NAMESPACE__.'\\Builtin\\ServiceProviderGenerator'
    ];

    /**
     * Available generators: builtin and user-defined
     * @var array
     */
    protected $generators = [];

    /**
     * @param  ConfigInterface $config
     * @param  ComposerJson    $composerJson
     */
    public function __construct(ConfigInterface $config, ComposerJson $composerJson)
    {
        $this->config = $config;
        $this->composerJson = $composerJson;
        $this->generators = array_merge(
            $this->getActivatedBuiltinGenerators(),
            $this->config->get('generators', [])
        );
        ksort($this->generators, SORT_NATURAL);
    }

    /**
     * Makes an instance of the appropriate generator
     *
     * @param  string        $templateName
     * @throws GeneratorException
     * @return GeneratorInterface
     */
    public function makeGenerator($templateName, $classPath, array $generatorOptions = []): GeneratorInterface
    {
        if (array_key_exists($templateName, $this->generators)) {
            $generator = $this->generators[$templateName];

            // Make sure the class we are about to instanciate implements GeneratorInterface
            if (!class_exists($generator)) {
                throw new GeneratorException(
                    sprintf('Class %s does not exist', $generator),
                    GeneratorException::GENERATOR_DOES_NOT_EXIST
                );
            }
            if (! in_array(GeneratorInterface::class, class_implements($generator))) {
                throw new GeneratorException(
                    sprintf('Generator %s is not an instance of %s', $generator, GeneratorInterface::class),
                    GeneratorException::NOT_GENERATOR_INSTANCE
                );
            }

            $classPath = $this->config->get('paths.'.$templateName, '') . $classPath;

            $instance = $this->getInstance($this->generators[$templateName], $classPath, $generatorOptions);
            $instance->setPermissions(
                $this->config->get('permissions.dir', 0755),
                $this->config->get('permissions.file', 0644)
            );
            return $instance;
        }

        throw new GeneratorException(
            sprintf('Could not find generator %s', $templateName),
            GeneratorException::GENERATOR_NOT_FOUND
        );
    }

    /**
     * Returns a new instance of the generator
     *
     * @param  string             $name
     * @param  string             $classPath
     * @return GeneratorInterface
     */
    protected function getInstance(string $name, string $filepath, array $generatorOptions = []): GeneratorInterface
    {
        $instance = new $name($this->config, $filepath, $generatorOptions);
        if ($instance instanceof ClassGeneratorInterface) {
            $instance->setComposerJson($this->composerJson);
        }

        return $instance;
    }

    /**
     * Returns the available generators
     *
     * @return array
     */
    public function getDefinedGenerators(): array
    {
        return $this->generators;
    }

    /**
     * Returns the activated builtin generators, depending on configuration
     *
     * @return array
     */
    protected function getActivatedBuiltinGenerators(): array
    {
        $active = $this->config->get('builtin-generators');
        if (null === $active) {
            return $this->builtinGenerators;
        }

        return array_filter($this->builtinGenerators, function ($gen) use ($active) {
            return in_array($gen, $active);
        }, ARRAY_FILTER_USE_KEY);
    }
}
