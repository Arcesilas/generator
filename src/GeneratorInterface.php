<?php

namespace Arcesilas\Generator;

interface GeneratorInterface
{
    /**
     * Returns the stub after data replacement
     * After that, it can be optionnally edited by user then written to file
     *
     * @return string
     */
    public function getStub(): string;

    /**
     * Check the file to create does not already exist
     *
     * @throws GeneratorException
     */
    public function checkFileDoesNotExist();

    /**
     * Define the permissions for newly created files and directories
     *
     * @param  integer        $dir  Directories permissions
     * @param  integer        $file Files permissions
     */
    public function setPermissions($dir = 0755, $file = 0644);

    /**
     * Write the content to file
     *
     * @param  string $content
     */
    public function write($content);
}
