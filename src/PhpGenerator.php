<?php
declare(strict_types=1);

namespace Arcesilas\Generator;

abstract class PhpGenerator extends BaseGenerator
{
    /**
     * {@inheritdoc}
     */
    public function getStub(): string
    {
        extract($this->stubData);
        ob_start();
        require $this->stub;
        return ob_get_clean();
    }
}
