<?php
declare(strict_types=1);

namespace Arcesilas\Generator;

abstract class RegexGenerator extends BaseGenerator
{
    /**
     * Returns the content of the template
     *
     * @return string
     */
    protected function getTemplate(): string
    {
        return file_get_contents($this->stub);
    }

    /**
     * {@inheritdoc}
     */
    public function getStub(): string
    {
        return preg_replace_callback('`({%\s*([a-zA-Z]+)\s*%})`', function ($matches) {
            $data = $this->stubData;
            return $data[$matches[2]] ?? $matches[1];
        }, $this->getTemplate());
    }
}
