<?php

namespace Arcesilas\Generator\Traits;

use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\Util\ComposerJsonException;
use Arcesilas\Generator\GeneratorException;

trait ClassGeneratorTrait
{
    /**
     * @var ComposerJson
     */
    protected $composerJson;

    public function setComposerJson(ComposerJson $composerJson)
    {
        $this->composerJson = $composerJson;
        try {
            $this->stubData = array_merge(
                $this->stubData,
                $this->composerJson->classinfo($this->stubData['filepath'])
            );
        } catch (ComposerJsonException $e) {
            throw new GeneratorException($e->getMessage(), GeneratorException::NAMESPACE_NOT_FOUND);
        }
    }
}
