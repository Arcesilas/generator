<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Util;

class ComposerJson
{
    /**
     * Content of `composer.json` file
     *
     * @var array
     */
    protected $content;

    /**
     * Directory of the composer.json file
     *
     * @var string
     */
    protected $root;

    /**
     * @param  string      $path Path to `composer.json` directory
     */
    public function __construct(string $path)
    {
        $this->root = realpath($path);
    }

    /**
     * Return an array with PSR-4 for autoload and autoload-dev. The array is exactly as defined in `composer.json`:
     *   namespace => directory
     *
     * @return array
     */
    public function getPsr4(): array
    {
        $this->readComposerJson();

        $psr4 = $this->content['autoload']['psr-4'] ?? [];
        $psr4Dev = $this->content['autoload-dev']['psr-4'] ?? [];

        return array_merge($psr4, $psr4Dev);
    }

    /**
     * Return information for the given classPath
     *
     * @param  string    $classPath
     * @return array
     *
     * @throws ComposerJsonException
     */
    public function classinfo(string $classPath): array
    {
        $psr4 = $this->getPsr4();

        foreach ($psr4 as $ns => $path) {
            if (strpos($classPath, $path) !== 0) {
                continue;
            }

            $ns = rtrim($ns, '\\');

            $classinfo = [];

            $path = rtrim($path, '\\/');
            $info = pathinfo($classPath);
            // Full path to file to generated
            $classinfo['filepath'] = $this->root.'/'.$classPath;
            // Full path to directory
            $classinfo['dirname'] = $this->root.'/'.$info['dirname'];
            // Class namespace
            $classinfo['namespace'] = str_replace([$path, '/'], [$ns, '\\'], $info['dirname']);
            // Class name
            $classinfo['class'] = $info['filename'];

            return $classinfo;
        }

        throw new ComposerJsonException('Could not find any namespace for classpath '.$classPath);
    }

    /**
     * Read the composer.json file and load its content
     *
     * @throws ComposerJsonException
     */
    protected function readComposerJson()
    {
        $path = rtrim($this->root, '\\/');
        $composerJson = $path.'/composer.json';;
        if (!file_exists($path.'/composer.json')) {
            throw new ComposerJsonException('File `composer.json` not found.');
        }

        $this->content = json_decode(file_get_contents($path.'/composer.json'), true);

        if (null === $this->content) {
            throw new ComposerJsonException('composer.json seems to be invalid');
        }
    }
}
