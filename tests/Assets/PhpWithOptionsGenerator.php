<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Tests\Assets;

use Arcesilas\Generator\PhpGenerator;
use Arcesilas\Generator\ClassGeneratorInterface;
use Arcesilas\Generator\Traits\ClassGeneratorTrait;
use Symfony\Component\Console\Input\InputOption;

class PhpWithOptionsGenerator extends PhpGenerator implements ClassGeneratorInterface
{
    use ClassGeneratorTrait;

    /**
     * {@inheritdoc}
     */
    public static $description = 'Generates a class with RegexGenerator using options';

    /**
     * {@inheritdoc}
     */
    protected $stub = __DIR__.'/PhpClassWithOptions.stub';
}
