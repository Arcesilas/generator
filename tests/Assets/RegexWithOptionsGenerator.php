<?php
declare(strict_types=1);

namespace Arcesilas\Generator\Tests\Assets;

use Arcesilas\Generator\RegexGenerator;
use Arcesilas\Generator\ClassGeneratorInterface;
use Arcesilas\Generator\Traits\ClassGeneratorTrait;

class RegexWithOptionsGenerator extends RegexGenerator implements ClassGeneratorInterface
{
    use ClassGeneratorTrait;

    /**
     * {@inheritdoc}
     */
    public static $description = 'Generates a class with RegexGenerator using options';

    /**
     * {@inheritdoc}
     */
    protected $stub = __DIR__.'/RegexClassWithOptions.stub';
}
