<?php

namespace Arcesilas\Generator\Tests\Assets;

use Arcesilas\Generator\RegexGenerator;

class SimpleFileGenerator
{
    protected $generatesClasses = false;

    protected $stub = __DIR__.'/SimpleFile.stub';
}
