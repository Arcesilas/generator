<?php

namespace Arcesilas\Generator\Tests\Feature;

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\Console\Tester\CommandTester;
use Arcesilas\Generator\Config\Config;
use Arcesilas\Generator\Console\Application;
use Arcesilas\Generator\Console\Command\MakeCommand;
use Arcesilas\Generator\GeneratorFactory;
use Arcesilas\Generator\Util\ComposerJson;

class BuiltinGeneratorsTest extends TestCase
{
    public function setUp()
    {
        vfsStream::setup('root');
        $this->root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root');

        $this->config = new Config($this->root->url().'/.generator.yml');
        $this->composerJson = new ComposerJson($this->root->url());
        $this->factory = new GeneratorFactory($this->config, $this->composerJson);
    }

    protected function getTester()
    {
        $application = new Application;
        $application->add(new MakeCommand($this->config, $this->factory));
        $command = $application->find('make');

        return new CommandTester($command);
    }

    public function argumentsProvider()
    {
        return [
            'ClassGenerator' => [
                'class',
                'app/FooBar',
                [],
                [
                    'namespace App;',
                    'class FooBar'
                ]
            ],
            'ClassGenerator: Abstract class' => [
                'class',
                'app/FooBar',
                [
                    'abstract'
                ],
                [
                    'namespace App;',
                    'abstract class FooBar'
                ]
            ],
            'ClassGenerator: Interface' => [
                'class',
                'app/FooBar',
                [
                    'interface'
                ],
                [
                    'namespace App;',
                    'interface FooBar'
                ]
            ],
            'ClassGenerator: trait' => [
                'class',
                'app/FooBar',
                [
                    'trait'
                ],
                [
                    'namespace App;',
                    'trait FooBar'
                ]
            ],
            'ClassGenerator: automatically detect Abstract class' => [
                'class',
                'app/AbstractFoo',
                [],
                [
                    'namespace App;',
                    'abstract class AbstractFoo'
                ]
            ],
            'ClassGenerator: automatically detect Interface' => [
                'class',
                'app/SimpleInterface',
                [],
                [
                    'namespace App;',
                    'interface SimpleInterface'
                ]
            ],
            'ClassGenerator: automatically detect Trait' => [
                'class',
                'app/SimpleTrait',
                [],
                [
                    'namespace App;',
                    'trait SimpleTrait'
                ]
            ],
            'CommandGenerator' => [
                'command',
                'app/SimpleCommand',
                [],
                [
                    'namespace App;',
                    'use Symfony\Component\Console\Command\Command;',
                    'class SimpleCommand extends Command',
                ]
            ],
            'PHPUnitTestCaseGenerator' => [
                'test',
                'tests/SimpleTest',
                [],
                [
                    'namespace App\\Tests;',
                    'use PHPUnit\Framework\TestCase;',
                    'class SimpleTest extends TestCase'
                ]
            ],
            'PhpGeneratorGenerator' => [
                'php-generator',
                'app/SimpleGenerator',
                [],
                [
                    'namespace App;',
                    'class SimpleGenerator extends PhpGenerator implements ClassGeneratorInterface'
                ]
            ],
            'RegexGeneratorGenerator' => [
                'regex-generator',
                'app/SimpleGenerator',
                [],
                [
                    'namespace App;',
                    'class SimpleGenerator extends RegexGenerator implements ClassGeneratorInterface'
                ]
            ],
            'ServiceProviderGenerator' => [
                'service-provider',
                'app/SimpleServiceProvider',
                [],
                [
                    'namespace App;',
                    'use League\Container\ServiceProvider\AbstractServiceProvider;',
                    'class SimpleServiceProvider extends AbstractServiceProvider'
                ]
            ],
            'Bootable ServiceProviderGenerator' => [
                'service-provider',
                'app/SimpleServiceProvider',
                [
                    'bootable'
                ],
                [
                    'namespace App;',
                    'use League\Container\ServiceProvider\AbstractServiceProvider;',
                    'class SimpleServiceProvider extends AbstractServiceProvider',
                    'public function boot()'
                ]
            ]

        ];
    }

    /**
     * @dataProvider argumentsProvider
     */
    public function testBuiltinGenerators($template, $name, $options, $expectedContents)
    {
        $tester = $this->getTester();
        $tester->execute([
            'command' => 'make',
            'template'=> $template,
            'name' => $name,
            '--option' => $options
        ]);

        $file = 'vfs://root/'.$name.'.php';
        $contents = file_get_contents($file);

        $this->assertFileExists($file);
        foreach ($expectedContents as $string) {
            $this->assertContains($string, $contents);
        }
    }
}
