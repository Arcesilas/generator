<?php

namespace Arcesilas\Generator\Tests\Feature;

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\Console\Tester\CommandTester;
use Arcesilas\Generator\Config\Config;
use Arcesilas\Generator\Console\Application;
use Arcesilas\Generator\Console\Command\MakeCommand;
use Arcesilas\Generator\GeneratorFactory;
use Arcesilas\Generator\Util\ComposerJson;

class GeneratorsWithOptionsTest extends TestCase
{
    public function setUp()
    {
        vfsStream::setup('root');
        $this->root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root');

        $this->config = new Config($this->root->url().'/.generator.yml');
        $this->composerJson = new ComposerJson($this->root->url());
        $this->factory = new GeneratorFactory($this->config, $this->composerJson);
    }

    protected function getTester()
    {
        $application = new Application;
        $application->add(new MakeCommand($this->config, $this->factory));
        $command = $application->find('make');

        return new CommandTester($command);
    }

    public function testPhpGeneratorWithOptions()
    {
        $tester = $this->getTester();
        $tester->execute([
            'command' => 'make',
            'template' => 'php-class-with-options',
            'name' => 'app/PhpClassWithOptions',
            '--option' => [
                'property:fooProp',
                'argument:fooArg',
                'condition',
                'conditionalValue:foo value'
            ]
        ]);

        $file = 'vfs://root/app/PhpClassWithOptions.php';
        $contents = file_get_contents($file);

        $this->assertFileExists($file);

        $expected = [
            'namespace App;',
            'class PhpClassWithOptions',
            'protected $fooProp;',
            'public function __construct($fooArg)',
            '$this->fooProp = $fooArg;',
            'public function conditionalMethod()',
            'return \'foo value\';',
        ];

        foreach ($expected as $string) {
            $this->assertContains($string, $contents);
        }

        $this->assertNotContains('public function UnexpectedMethod()', $contents);
    }

    public function testRegexGeneratorWithOptions()
    {
        $tester = $this->getTester();
        $tester->execute([
            'command' => 'make',
            'template' => 'regex-class-with-options',
            'name' => 'app/RegexClassWithOptions',
            '--option' => [
                'property:fooProp',
                'argument:fooArg'
            ]
        ]);

        $file = 'vfs://root/app/RegexClassWithOptions.php';
        $contents = file_get_contents($file);

        $this->assertFileExists($file);

        $expected = [
            'namespace App;',
            'class RegexClassWithOptions',
            'protected $fooProp;',
            'public function __construct($fooArg)',
            '$this->fooProp = $fooArg;'
        ];

        foreach ($expected as $string) {
            $this->assertContains($string, $contents);
        }
    }
}
