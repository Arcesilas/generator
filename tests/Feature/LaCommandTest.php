<?php

namespace Arcesilas\Generator\Tests\Feature;

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;
use Arcesilas\Generator\Console\Command\LaCommand;
use Arcesilas\Generator\Console\Command\ListCommand;
use Arcesilas\Generator\Console\Application;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\GeneratorFactory;
use Arcesilas\Generator\Config\Config;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Tester\CommandTester;

class LaCommandTest extends TestCase
{
    public function testLaCommand()
    {
        vfsStream::setup('root');
        $root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root');

        $config = new Config($root->url().'/.generator.yml');
        $composerJson = new ComposerJson($root->url());
        $factory = new GeneratorFactory($config, $composerJson);

        $application = new Application;

        $application->add(new ListCommand($config, $factory));
        $application->add(new LaCommand);
        $listCommand = $application->find('list');

        $listTester = new CommandTester($listCommand);

        $laCommand = $application->find('la');
        $laTester = new CommandTester($laCommand);

        $listTester->execute(['--all' => true]);
        $laTester->execute([]);

        $this->assertSame($listTester->getDisplay(), $laTester->getDisplay());
    }
}
