<?php

namespace Arcesilas\Generator\Tests\Feature;

use PHPUnit\Framework\TestCase;
use Arcesilas\Generator\Config\Config;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\GeneratorFactory;
use Arcesilas\Generator\Console\Application;
use Arcesilas\Generator\Console\Command\ListCommand;
use Symfony\Component\Console\Tester\CommandTester;
use org\bovigo\vfs\vfsStream;

class ListCommandTest extends TestCase
{
    protected $tester;

    public function setUp()
    {
        vfsStream::setup('root');
        $root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root');

        $config = new Config($root->url().'/.generator.yml');
        $composerJson = new ComposerJson($root->url());
        $factory = new GeneratorFactory($config, $composerJson);

        $application = new Application;

        $application->add(new ListCommand($config, $factory));
        $command = $application->find('list');

        $this->tester = new CommandTester($command);
    }

    public function testGetList()
    {
        $this->tester->execute([]);
        $this->assertEquals(0, $this->tester->getStatusCode());

        $output = $this->tester->getDisplay();
        $this->assertRegexp('`command\s+Arcesilas\\\\Generator\\\\Builtin\\\\CommandGenerator`', $output);
        $this->assertRegexp('`test\s+Arcesilas\\\\Generator\\\\Builtin\\\\PHPUnitTestCaseGenerator`', $output);
    }

    public function testGetListWithOptionAll()
    {
        $this->tester->execute(['--all' => true]);
        $this->assertEquals(0, $this->tester->getStatusCode());

        $output = $this->tester->getDisplay();

        $this->assertRegexp('`✔\s+command\s+Arcesilas\\\\Generator\\\\Builtin\\\\CommandGenerator\s+\w+`', $output);
        $this->assertRegexp('`✔\s+test\s+Arcesilas\\\\Generator\\\\Builtin\\\\PHPUnitTestCaseGenerator\s+\w+`', $output);
        // Invalid generators have no description
        $this->assertRegexp('`✘\s+bar\s+Arcesilas\\\\Generator\\\\Tests\\\\Assets\\\\NotAGenerator\s+$`m', $output);
        $this->assertRegexp('`✘\s+foo\s+Foo\\\\UnknownClass\s+$`m', $output);
    }
}
