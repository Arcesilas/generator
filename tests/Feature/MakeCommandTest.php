<?php

namespace Arcesilas\Generator\Tests\Feature;

use PHPUnit\Framework\TestCase;
use Arcesilas\Generator\Console\Application;
use Arcesilas\Generator\Console\Command\MakeCommand;
use Arcesilas\Generator\Config\Config;
use Arcesilas\Generator\GeneratorFactory;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\GeneratorException;
use Symfony\Component\Console\Tester\CommandTester;
use org\bovigo\vfs\vfsStream;

class MakeCommandTest extends TestCase
{
    protected $root;

    protected $config;

    protected $factory;

    protected $composerJson;

    public function setUp()
    {
        vfsStream::setup('root');
        $this->root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root');

        $this->config = new Config($this->root->url().'/.generator.yml');
        $this->composerJson = new ComposerJson($this->root->url());
        $this->factory = new GeneratorFactory($this->config, $this->composerJson);
    }

    protected function getTester()
    {
        $application = new Application;
        $application->add(new MakeCommand($this->config, $this->factory));
        $command = $application->find('make');

        return new CommandTester($command);
    }

    public function testMakeCommandSuccess()
    {
        $tester = $this->getTester();
        $tester->execute([
            'command' => 'make',
            'template' => 'command',
            'name' => 'app/Console/Commands/ExampleCommand',
            '--suffix' => '.php7',
            '--edit' => true,
            '--editor' => __DIR__.'/../Assets/root/bin/editor',
        ]);

        $output = $tester->getDisplay();
        $this->assertContains('[OK]', $output);

        $this->assertFileExists('vfs://root/app/Console/Commands/ExampleCommand.php7');
    }

    public function testThrowExceptionWhenEditorNotFound()
    {
        $tester = $this->getTester();
        $tester->execute([
            'command' => 'make',
            'template' => 'command',
            'name' => 'app/Console/Commands/ExampleCommand',
            '--suffix' => '.php7',
            '--edit' => true,
        ]);

        $output = $tester->getDisplay();
        $this->assertContains('[ERROR]', $output);
        $this->assertEquals(GeneratorException::EDITOR_NOT_FOUND, $tester->getStatusCode());
    }

    /**
     * @dataProvider failingNamesProvider
     */
    public function testFails($template, $name, $code)
    {
        $unwritable = $this->root->getChild('app/Unwritable');
        $unwritable->chmod(0000);

        $tester = $this->getTester();

        $tester->execute([
            'command' => 'make',
            'template' => $template,
            'name' => $name
        ]);
        $output = $tester->getDisplay();
        $this->assertContains('[ERROR]', $output);

        $this->assertEquals($code, $tester->getStatusCode());

        // Not sure it's best practise...
        // But don't want to make one separate test just for that
        if ($code !== GeneratorException::FILE_ALREADY_EXISTS) {
            $this->assertFileNotExists('vfs://root/'.$name.'.php');
        }
    }

    public function failingNamesProvider()
    {
        return [
            ['bar', 'app/Console/Commands/ExampleCommand', GeneratorException::NOT_GENERATOR_INSTANCE],
            ['baz', 'app/Console/Commands/ExampleCommand', GeneratorException::GENERATOR_NOT_FOUND],
            ['command', 'app/Already/ExistClass', GeneratorException::FILE_ALREADY_EXISTS],
            ['command', 'app/Unwritable/ExampleClass', GeneratorException::FILE_NOT_WRITTEN],
            ['command', 'app/Unwritable/Example/ExampleClass', GeneratorException::DIRECTORY_NOT_CREATED],
            ['command', 'unknown/Example/ExampleClass', GeneratorException::NAMESPACE_NOT_FOUND],
            ['foo', 'app/Console/Commands/ExampleCommand', GeneratorException::GENERATOR_DOES_NOT_EXIST]
        ];
    }

    public function testFailEditTempFile()
    {
        $tester = $this->getTester();

        $tester->execute([
            'command' => 'make',
            'template' => 'command',
            'name' => 'app/Console/Commands/ExampleCommand',
            '--suffix' => '.php7',
            '--edit' => true,
            '--editor' => __DIR__.'/../Assets/root/bin/failing-editor',
        ]);

        $output = $tester->getDisplay();

        $this->assertEquals(0, $tester->getStatusCode());
        $this->assertContains('[WARNING]', $output);
        $this->assertContains('Exit Code: 126', $output);
    }

    public function testMakeWithDefaultPath()
    {
        $this->root = vfsStream::copyFromFileSystem(__DIR__.'/../Assets/root2');
        $this->config = new Config($this->root->url().'/.generator.yml');
        $this->composerJson = new ComposerJson($this->root->url());
        $this->factory = new GeneratorFactory($this->config, $this->composerJson);

        $tester = $this->getTester();

        $tester->execute([
            'command' => 'make',
            'template' => 'class',
            'name' => 'FooClass'
        ]);

        $output = $tester->getDisplay();
        $this->assertContains('[OK]', $output);

        $this->assertFileExists('vfs://root/src/FooClass.php');
    }
}
