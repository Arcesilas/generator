<?php

namespace Arcesilas\Generator\Tests\Unit;

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\Util\ComposerJsonException;

class ComposerJsonTest extends TestCase
{
    protected $root;

    protected $composerJson;

    public function setUp()
    {
        $this->root = vfsStream::setup('root');
        $file = vfsStream::newFile('composer.json')
            ->at($this->root)
            ->setContent('{
                "autoload": {
                    "psr-4": {
                        "App\\\\": "app/"
                    }
                },
                "autoload-dev": {
                    "psr-4": {
                        "App\\\\Tests\\\\": "tests/"
                    }
                }
            }');
    }

    public function testGetPsr4()
    {
        $composerJson = new ComposerJson($this->root->url());

        $expected = [
            'App\\' => 'app/',
            'App\\Tests\\' => 'tests/'
        ];

        $this->assertEquals($expected, $composerJson->getPsr4());
    }

    /**
     * @dataProvider successfullClassinfoProvider
     */
    public function testClassinfo($classpath, $expected)
    {
        $composerJson = new ComposerJson($this->root->url());

        $classinfo = $composerJson->classinfo($classpath);

        $this->assertEquals($expected, $classinfo);
    }

    public function successfullClassinfoProvider()
    {
        return [
            [
                'app/Example/ExampleClass',
                [
                    'filepath' => 'vfs://root/app/Example/ExampleClass',
                    'dirname' => 'vfs://root/app/Example',
                    'namespace' => 'App\\Example',
                    'class' => 'ExampleClass'
                ]
            ],
            [
                'tests/Other/OtherClass',
                [
                    'filepath' => 'vfs://root/tests/Other/OtherClass',
                    'dirname' => 'vfs://root/tests/Other',
                    'namespace' => 'App\\Tests\\Other',
                    'class' => 'OtherClass'
                ]
            ]
        ];
    }

    public function testThrowAnExceptionWhenPsr4Fails()
    {
        $this->expectException(ComposerJsonException::class);

        (new ComposerJson($this->root->url()))->classinfo('unknown/Namespace/NonExistantClass');
    }

    public function testThrowsExceptionWhenNoComposerJson()
    {
        $this->root = VfsStream::setup('root');

        $this->expectException(ComposerJsonException::class);

        (new ComposerJson($this->root->url()))->getPsr4();
    }

    public function testThrowsExceptionOnInvalidComposerJson()
    {
        $this->root = VfsStream::setup('root');
        $file = vfsStream::newFile('composer.json')
            ->at($this->root)
            ->setContent('{
                "autoload": {
                    "psr-4": {
                        "App\\\\": "app/"
                    }
                },
            }');

        $this->expectException(ComposerJsonException::class);

        (new ComposerJson($this->root->url()))->getPsr4();
    }
}
