<?php

namespace Arcesilas\Generator\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Arcesilas\Generator\Config\getcwd;
use Arcesilas\Generator\Config\Config;
use org\bovigo\vfs\vfsStream;
use Dflydev\DotAccessData\Data;

class ConfigTest extends TestCase
{
    protected $root;

    protected $config;

    protected function setupVfs()
    {
        $this->root = vfsStream::setup('root');

        $configFile = vfsStream::newFile('.generator.yml')
            ->at($this->root)
            ->setContent('{}');

        $this->config = new Config($configFile->url());
    }

    /**
     * @dataProvider pathsProvider
     */
    public function testInstanceWithDefaultPath($filepath, $expected, $times)
    {
        $this->setupVfs();
        $config = $this->getMockBuilder(Config::class)
            ->setMethods(['addConfig'])
            ->disableOriginalConstructor()
            ->getMock();

        $times = $times ? $this->once() : $this->exactly(0);

        $config->expects($times)
            ->method('addConfig')
            ->with($this->equalTo($expected));

        $ref = new \ReflectionClass(Config::class);
        $refConstructor = $ref->getConstructor();
        $refConstructor->invoke($config, $filepath);
    }

    public function pathsProvider()
    {
        // function getcwd() has been mocked in bootstrap/functions.php to return vfs://root
        return [
            [null, 'vfs://root/.generator.yml', 1],
            ['/path/to/config.yml', '/path/to/config.yml', 0]
        ];
    }

    public function testAddConfig()
    {
        $this->setupVfs();
        $additionalConfig = vfsStream::newFile('config.yml')
            ->at($this->root)
            ->setContent('{"foo": "bar"}');

        $this->config->addConfig($additionalConfig->url());

        $refData = $this->getRefData();
        $data = $refData->getValue($this->config);

        $this->assertEquals('bar', $data->get('foo'));
    }

    public function testGet()
    {
        $this->setupVfs();
        $refData = $this->getRefData();

        $data = $this->createMock(Data::class);
        $data->expects($this->exactly(2))
            ->method('get')
            ->with($this->equalTo('foo'))
            ->willReturn('bar');

        $refData->setValue($this->config, $data);

        $this->assertEquals('bar', $this->config->get('foo'));
        $this->assertEquals('bar', $this->config->foo);
    }

    /**
     * @dataProvider setDataProvider
     */
    public function testSet($times, $array)
    {
        $this->setupVfs();
        $refData = $this->getRefData();

        $data = $this->createMock(Data::class);
        $data->expects($this->exactly($times))
            ->method('set')
            ->withConsecutive($this->equalTo(array_keys($array)));

        $refData->setValue($this->config, $data);

        $this->config->set($array);
    }

    public function setDataProvider()
    {
        return [
            [1, ['foo' => 'bar']],
            [2, ['foo' => 'bar', 'goo' => 'baz']]
        ];
    }

    protected function getRefData()
    {
        $ref = new \ReflectionObject($this->config);
        $refData = $ref->getProperty('data');
        $refData->setAccessible(true);
        return $refData;
    }
}
