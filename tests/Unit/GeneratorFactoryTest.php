<?php

namespace Arcesilas\Generator\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Arcesilas\Generator\Config\Config;
use Arcesilas\Generator\Util\ComposerJson;
use Arcesilas\Generator\GeneratorInterface;
use Arcesilas\Generator\GeneratorFactory;

class GeneratorFactoryTest extends TestCase
{
    public function testInstance()
    {
        $config = $this->createMock(Config::class);
        $configValueMap = [
            ['builtin-generators', [], []],
            ['generators', [], []]
        ];

        $config->expects($this->exactly(2))
            ->method('get')
            ->will($this->returnValueMap($configValueMap));

        $composerJson = $this->createMock(ComposerJson::class);

        $factory = new GeneratorFactory($config, $composerJson);

        $ref = new \ReflectionObject($factory);
        $refProp = $ref->getProperty('generators');
        $refProp->setAccessible(true);

        $expected = [
            'class' => 'Arcesilas\\Generator\\Builtin\\ClassGenerator',
            'command' => 'Arcesilas\\Generator\\Builtin\\CommandGenerator',
            'php-generator' => 'Arcesilas\\Generator\\Builtin\\PhpGeneratorGenerator',
            'regex-generator' => 'Arcesilas\\Generator\\Builtin\\RegexGeneratorGenerator',
            'service-provider' => 'Arcesilas\\Generator\\Builtin\\ServiceProviderGenerator',
            'test' => 'Arcesilas\\Generator\\Builtin\\PHPUnitTestCaseGenerator',
        ];

        $this->assertEquals($expected, $refProp->getValue($factory));
    }

    public function testGetActivatedBuiltinGenerators()
    {
        $config = $this->createMock(Config::class);
        $config->expects($this->any())
            ->method('get')
            ->with('builtin-generators')
            ->willReturn(['class', 'command']);

        $composerJson = $this->createMock(ComposerJson::class);

        $factory = $this->getMockBuilder(GeneratorFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ref = new \ReflectionObject($factory);

        $refConfig = $ref->getProperty('config');
        $refConfig->setAccessible(true);
        $refConfig->setValue($factory, $config);

        $refComposerJson = $ref->getProperty('composerJson');
        $refComposerJson->setAccessible(true);
        $refComposerJson->setValue($factory, $composerJson);

        $refMethod = $ref->getMethod('getActivatedBuiltinGenerators');
        $refMethod->setAccessible(true);

        $expected = [
            'class' => 'Arcesilas\\Generator\\Builtin\\ClassGenerator',
            'command' => 'Arcesilas\\Generator\\Builtin\\CommandGenerator',
        ];

        $this->assertEquals($expected, $refMethod->invoke($factory));
    }

    public function testItMakesAConfiguredGenerator()
    {
        $configValueMap = [
            ['generators', [], []],
            ['permissions.dir', 0755, 0755],
            ['permissions.file', 0755, 0640]
        ];

        $config = $this->createMock(Config::class);
        $config->expects($this->exactly(5))
            ->method('get')
            ->will($this->returnValueMap($configValueMap));

        $composerJson = $this->createMock(ComposerJson::class);

        $factory = new GeneratorFactory($config, $composerJson);

        $generator = $factory->makeGenerator('command', 'Example/ExampleClass');
        $this->assertInstanceOf(GeneratorInterface::class, $generator);
    }
}
