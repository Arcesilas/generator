<?php

namespace Arcesilas\Generator\Util {

    function realpath($path)
    {
        return $path;
    }
}

namespace Arcesilas\Generator\Config {

    function getcwd()
    {
        return 'vfs://root';
    }
}

namespace Arcesilas\Generator\Console\Command {

    function getenv($name)
    {
        if ('EDITOR' === $name) {
            return null;
        }
        return \getenv($name);
    }
}
